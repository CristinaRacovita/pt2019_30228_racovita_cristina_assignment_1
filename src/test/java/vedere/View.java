package vedere;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class View extends JFrame {

	JButton aduna = new JButton(" + ");
	JButton scade = new JButton(" - ");
	JButton imp = new JButton(" / ");
	JButton der = new JButton(" ( polinom1 )' ");
	JButton integr = new JButton(" ∫(polinom1)dx ");
	JButton inm = new JButton(" * ");

	JTextField p1 = new JTextField(20);
	JTextField p2 = new JTextField(20);
	
	JLabel text1 = new JLabel ("Polinom nr. 1 : ");
	JLabel text2 = new JLabel ("Polinom nr. 2 : ");

	JTextField rez = new JTextField(20);
	JLabel res = new JLabel("Rezultat : ");
	
	public View(){
		
		this.setTitle("Operatii cu polinoame");
		this.setSize(600, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		
		panel1.add(p1);
		panel2.add(p2);
		
		JPanel pn1 = new JPanel();
		JPanel pn2 = new JPanel();
		JPanel pn3 = new JPanel();
		JPanel pn4 = new JPanel();
		JPanel pn5 = new JPanel();
		JPanel pn6 = new JPanel();
		JPanel pn7 = new JPanel();
		JPanel pn8 = new JPanel();
		JPanel pn9 = new JPanel();
		JPanel pn10 = new JPanel();
		JPanel pn11 = new JPanel();
		JPanel pn12 = new JPanel();
		JPanel pn13 = new JPanel();

		rez.setEditable(false);
		panel3.add(rez);
		
		pn5.add(aduna);
		pn6.add(scade);
		pn7.add(imp);
		pn8.add(inm);
		pn9.add(integr);
		pn10.add(der);
		
		pn1.add(text1);
		pn1.add(panel1);
		pn1.setLayout(new BoxLayout(pn1,BoxLayout.Y_AXIS));
		
		pn2.add(text2);
		pn2.add(panel2);
		pn2.setLayout(new BoxLayout(pn2,BoxLayout.Y_AXIS));
		
		pn3.add(pn1);
		pn3.add(pn2);
		pn3.setLayout(new BoxLayout(pn3,BoxLayout.X_AXIS));
		
		pn4.add(pn5);
		pn4.add(pn6);
		pn4.add(pn8);
		pn4.setLayout(new BoxLayout(pn4,BoxLayout.X_AXIS));
		
		pn11.add(pn7);
		pn11.add(pn10);
		pn11.add(pn9);
		pn11.setLayout(new BoxLayout(pn11,BoxLayout.X_AXIS));
		
		pn12.add(res);
		pn12.add(panel3);
		pn12.setLayout(new BoxLayout(pn12,BoxLayout.Y_AXIS));

		pn13.add(pn3);
		pn13.add(pn4);
		pn13.add(pn11);
		pn13.add(pn12);
		pn13.setLayout(new BoxLayout(pn13,BoxLayout.Y_AXIS));
		
		this.add(pn13);
		
	}
	

	public void addListener(ActionListener ButtonListener) {
		aduna.addActionListener(ButtonListener);
	}

	public void subListener(ActionListener ButtonListener) {
		scade.addActionListener(ButtonListener);
	}
	public void derivListener(ActionListener ButtonListener) {
		der.addActionListener(ButtonListener);
	}
	public void impListener(ActionListener ButtonListener) {
		imp.addActionListener(ButtonListener);
	}
	public void inmListener(ActionListener ButtonListener) {
		inm.addActionListener(ButtonListener);
	}
	public void integListener(ActionListener ButtonListener) {
		integr.addActionListener(ButtonListener);
	}

	public String getTextPol1() {
		return p1.getText();
	}
	public String getTextPol2() {
		return p2.getText();
	}
	public void setTextRez(String s) {
		rez.setText(s);
	}
	
}
