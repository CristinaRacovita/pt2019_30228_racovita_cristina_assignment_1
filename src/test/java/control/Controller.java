package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import model.Polinom;
import vedere.View;

public class Controller {

	View view = new View();

	public Controller(View v) {
		this.view = v;

		this.view.addListener(new AdunaListener());
		this.view.subListener(new SubbListener());
		this.view.derivListener(new DerListener());
		this.view.integListener(new IntListener());
		this.view.inmListener(new InmulListener());
		this.view.impListener(new ImpListener());
	}

	class AdunaListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Polinom pol1 = new Polinom();
			Polinom pol2 = new Polinom();

			String s1 = view.getTextPol1();
			String s2 = view.getTextPol2();
			pol1 = Polinom.messageToPolynomial(s1);
			pol2 = Polinom.messageToPolynomial(s2);
			Polinom rezultat = new Polinom();

			if (pol1 != null && pol2 != null) {

				rezultat = pol1.adunare(pol2);
				String rez = rezultat.polynomialToMessage().toString();
				view.setTextRez(rez);
			} else {
				JOptionPane.showMessageDialog(view,
						"Te rog sa reintroduci polinoamele dupa forma : \n                       Ax^2+Bx^1+C", "EROARE",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	class SubbListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Polinom pol1 = new Polinom();
			Polinom pol2 = new Polinom();

			String s1 = view.getTextPol1();
			String s2 = view.getTextPol2();
			pol1 = Polinom.messageToPolynomial(s1);
			pol2 = Polinom.messageToPolynomial(s2);
			Polinom rezultat = new Polinom();

			if (pol1 != null && pol2 != null) {
				rezultat = pol1.scadere(pol2);
				String rez = rezultat.polynomialToMessage().toString();
				view.setTextRez(rez);
			} else

			{
				JOptionPane.showMessageDialog(view,
						"Te rog sa reintroduci polinoamele dupa forma : \n                       Ax^2+Bx^1+C", "EROARE",
						JOptionPane.INFORMATION_MESSAGE);

			}

		}
	}

	class DerListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Polinom pol1 = new Polinom();
			// Polinom pol2 = new Polinom();

			String s1 = view.getTextPol1();
			// String s2 = view.getTextPol2();
			pol1 = Polinom.messageToPolynomial(s1);
			// pol2 = Model.MessageToPolynomial(s2);
			Polinom rezultat = new Polinom();

			if (pol1 != null) {
				rezultat = pol1.derivare();
				String rez = rezultat.polynomialToMessage().toString();
				view.setTextRez(rez);
			} else {
				JOptionPane.showMessageDialog(view,
						"Te rog sa reintroduci polinoamele dupa forma : \n                       Ax^2+Bx^1+C", "EROARE",
						JOptionPane.INFORMATION_MESSAGE);

			}

		}
	}

	class IntListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Polinom pol1 = new Polinom();
			// Polinom pol2 = new Polinom();

			String s1 = view.getTextPol1();
			// view.text2.setEditable(false);
			pol1 = Polinom.messageToPolynomial(s1);
			// pol2 = Model.MessageToPolynomial(s2);
			Polinom rezultat = new Polinom();

			if (pol1 != null) {
				rezultat = pol1.integrare();
				String rez = rezultat.polynomialToMessage().toString();
				view.setTextRez(rez);
			} else {
				JOptionPane.showMessageDialog(view,
						"Te rog sa reintroduci polinoamele dupa forma : \n                       Ax^2+Bx^1+C", "EROARE",
						JOptionPane.INFORMATION_MESSAGE);

			}

		}
	}

	class InmulListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Polinom pol1 = new Polinom();
			Polinom pol2 = new Polinom();

			String s1 = view.getTextPol1();
			String s2 = view.getTextPol2();
			pol1 = Polinom.messageToPolynomial(s1);
			pol2 = Polinom.messageToPolynomial(s2);
			Polinom rezultat = new Polinom();

			if (pol1 != null && pol2 != null) {

				rezultat = pol1.inmultire(pol2);
				String rez = rezultat.polynomialToMessage().toString();
				view.setTextRez(rez);
			}

			else {
				JOptionPane.showMessageDialog(view,
						"Te rog sa reintroduci polinoamele dupa forma : \n                       Ax^2+Bx^1+C", "EROARE",
						JOptionPane.INFORMATION_MESSAGE);

			}

		}
	}

	class ImpListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Polinom pol1 = new Polinom();
			Polinom pol2 = new Polinom();

			String s1 = view.getTextPol1();
			String s2 = view.getTextPol2();
			pol1 = Polinom.messageToPolynomial(s1);
			pol2 = Polinom.messageToPolynomial(s2);
			Polinom rezultat = new Polinom();

			if (pol1 != null && pol2 != null) {

				rezultat = pol1.div(pol2);
				String rez = rezultat.polynomialToMessage().toString();
				view.setTextRez(rez);
			}

			else {
				JOptionPane.showMessageDialog(view,
						"Te rog sa reintroduci polinoamele dupa forma : \n                       Ax^2+Bx^1+C", "EROARE",
						JOptionPane.INFORMATION_MESSAGE);

			}

		}
	}
}
