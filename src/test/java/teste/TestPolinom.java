package teste;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Polinom;

public class TestPolinom {

		@Test
		public void testAdd() {
			Polinom p1 = new Polinom("1x^2+2x^1+1");
			Polinom p2 = new Polinom("1x^1+1");
			Polinom rez = new Polinom();
			rez = p1.adunare(p2);
			System.out.println("******ADUNARE"+rez.polynomialToMessage()+"**********");
			assertEquals("1.0x^2 + 3.0x^1 + 2.0 ", rez.polynomialToMessage().toString());
		}
		@Test
		public void testSubb() {
			Polinom p1 = new Polinom("1x^2+2x^1+1");
			Polinom p2 = new Polinom("1x^1+1");
			Polinom rez = new Polinom();
			rez = p1.scadere(p2);
			System.out.println("******SCADERE"+rez.polynomialToMessage()+"**********");
			assertEquals("1.0x^2 + 1.0x^1 + 0.0 ", rez.polynomialToMessage().toString());
		}
		@Test
		public void testInm() {
			Polinom p1 = new Polinom("1x^2+2x^1+1");
			Polinom p2 = new Polinom("1x^1+1");
			Polinom rez = new Polinom();
			rez = p1.inmultire(p2);
			System.out.println("******INMULTIRE"+rez.polynomialToMessage()+"**********");
			assertEquals("1.0x^3 + 3.0x^2 + 3.0x^1 + 1.0 ", rez.polynomialToMessage().toString());
		}
		@Test
		public void testImp() {
			Polinom p1 = new Polinom("1x^2+2x^1+1");
			Polinom p2 = new Polinom("1x^1+1");
			Polinom rez = new Polinom();
			rez = p1.div(p2);
			System.out.println("******IMPARTIRE"+rez.polynomialToMessage()+"**********");
			assertEquals("1.0x^1 + 1.0 ", rez.polynomialToMessage().toString());
		}
		@Test
		public void testDer() {
			Polinom p1 = new Polinom("1x^2+2x^1+1");
			//Polinom p2 = new Polinom("1x^1+1");
			Polinom rez = new Polinom();
			rez = p1.derivare();
			System.out.println("******DERIVARE"+rez.polynomialToMessage()+"**********");
			assertEquals("2.0x^1 + 2.0  + 0.0 ", rez.polynomialToMessage().toString());
		}
		@Test
		public void testIntegr() {
			Polinom p1 = new Polinom("1x^2+2x^1+1");
			//Polinom p2 = new Polinom("1x^1+1");
			Polinom rez = new Polinom();
			rez = p1.integrare();
			System.out.println("******INTEGRARE"+rez.polynomialToMessage()+"**********");
			assertEquals("0.3x^3 + 1.0x^2 + 1.0x^1", rez.polynomialToMessage().toString());
		}
		
		
		
		
		
		

}
