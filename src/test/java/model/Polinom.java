package model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

public class Polinom {

	private ArrayList<Monom> monoame;

	public Polinom() {
		this.monoame = new ArrayList<Monom>();
	}
	
	public Polinom(String s) {
		Polinom p = new Polinom();
		p=messageToPolynomial(s);
		this.monoame=p.monoame;
	}

	public ArrayList<Monom> getMonoame() {
		return monoame;
	}

	public void setMonoame(ArrayList<Monom> monoame) {
		this.monoame = monoame;
	}

	public Polinom adunare(Polinom p1) {
		Polinom p3 = p1.comprimare();
		this.comprimare();
		Polinom rezultat = new Polinom();
		for (Monom m1 : p3.monoame) {
			rezultat.monoame.add(new Monom(m1.getValoare(), m1.getGrad()));
		}
		for (Monom m2 : this.monoame) {
			rezultat.monoame.add(new Monom(m2.getValoare(), m2.getGrad()));
		}
		rezultat = rezultat.comprimare();

		return rezultat;
	}

	public Polinom scadere(Polinom p1) {
		Polinom rezultat = new Polinom();

		for (Monom m1 : this.monoame) {
			rezultat.monoame.add(new Monom(m1.getValoare(), m1.getGrad()));
		}
		for (Monom m2 : p1.monoame) {
			float v = m2.getValoare();
			m2.setValoare(-v);
			rezultat.monoame.add(m2);
		}
		rezultat = rezultat.comprimare();
		return rezultat;
	}

	public Polinom derivare() {
		Polinom rezultat = new Polinom();

		for (Monom m : this.monoame) {
			if (m.getGrad() != 0) {
				int g = m.getGrad();
				m.setValoare(m.getValoare() * g);
				m.setGrad(g - 1);

			} else {
				m.setValoare(0);
			}
			rezultat.monoame.add(m);
		}
		return rezultat;
	}

	public Polinom integrare() {
		Polinom rezultat = new Polinom();

		for (Monom m : this.monoame) {
			if (m.getGrad() != 0) {
				int deg = m.getGrad();
				m.setGrad(deg + 1);
				m.setValoare(m.getValoare() / (float) m.getGrad());
			} else {
				int deg = m.getGrad();
				m.setGrad(deg + 1);
			}
			rezultat.monoame.add(m);
		}
		return rezultat;
	}

	public Polinom comprimare() {
		Polinom rezultat = new Polinom();
		Collections.sort(this.monoame, new ComparatorGrade());

		boolean ok = false;

		for (Monom m : this.monoame) {
			ok = false;
			for (Monom n : rezultat.monoame) {
				if (m.getGrad() == n.getGrad()) {
					n.setValoare(n.getValoare() + m.getValoare());
					ok = true;
				}
			}
			if (!ok) {
				rezultat.monoame.add(m);
			}

		}
		return rezultat;
	}

	public Polinom inmultire(Polinom p1) {
		Polinom rezultat = new Polinom();
		Polinom aux = new Polinom();

		for (Monom m1 : p1.monoame) {
			for (Monom m2 : this.monoame) {
				Monom m = new Monom(m1.getValoare() * m2.getValoare(), m1.getGrad() + m2.getGrad());
				aux.monoame.add(m);
			}
		}
		rezultat = aux.comprimare();
		return rezultat;
	}

	static int gradPolinom(Polinom p) {
		Collections.sort(p.monoame, new ComparatorGrade());
		int i = 0;
		while (i < p.monoame.size()) {
			if (p.monoame.get(i).getValoare() != 0)
				return p.monoame.get(i).getGrad();
			i++;
		}
		return 0;
	}

	public boolean esteZero() {
		boolean ok = true;
		int k = 0;
		for (Monom m : this.monoame) {
			if (m.getGrad() == 0 && m.getValoare() == 0)
				k++;
		}

		if (k != this.monoame.size())
			ok = false;
		else
			ok = true;
		return ok;
	}

	boolean isValoareZero(int i) {
		boolean ok = false;

		if (this.monoame.get(i).getValoare() == 0) {
			ok = true;
		}

		return ok;
	}

	public ArrayList<Polinom> impartire(Polinom p) {
		ArrayList<Polinom> rezultat = new ArrayList<Polinom>();
		Polinom rest = new Polinom();
		Polinom cat = new Polinom();
		rest = this;
		if (p.esteZero())
			return null;
		Collections.sort(this.monoame, new ComparatorGrade());
		Collections.sort(p.monoame, new ComparatorGrade());

		while (!rest.esteZero() && gradPolinom(rest) >= gradPolinom(p)) {
			int i = 0;
			while (rest.isValoareZero(i)) {
				i++;
			}
			Monom t = new Monom(rest.monoame.get(i).getValoare() / p.monoame.get(0).getValoare(),
					rest.monoame.get(i).getGrad() - p.monoame.get(0).getGrad());
			Polinom aux = new Polinom();
			aux.monoame.add(t);
			cat.monoame.add(t);
			Polinom newRest = rest.scadere(aux.inmultire(p));
			rest = newRest;
			aux.monoame.clear();
		}
		rezultat.add(cat);
		rezultat.add(rest);
		return rezultat;
	}

	public Polinom div(Polinom p) {
		return this.impartire(p).get(0);
	}

	public static ArrayList<Integer> coeficienti(String s) {
		ArrayList<Integer> coeficienti = new ArrayList<Integer>();

		String[] coef = s.split("x\\^\\d+\\+?");
		for (String str : coef) {
			coeficienti.add(Integer.parseInt(str));
		}

		return coeficienti;
	}

	public static ArrayList<Integer> puteri(String s) {
		ArrayList<Integer> puteri = new ArrayList<Integer>();

		String[] termeni = s.split("(-|\\+)");
		for (String term : termeni) {
			String[] parti = term.split("\\^");
			if (parti[0] != " ") {
				if (parti.length > 1) {
					puteri.add(Integer.parseInt(parti[1]));
				} else {
					puteri.add(0);
				}
			}
		}

		return puteri;
	}

	public static Polinom messageToPolynomial(String s) {
		Polinom rez = new Polinom();
		try {
			ArrayList<Integer> coeficienti = coeficienti(s);
			ArrayList<Integer> puteri = puteri(s);
			int size = coeficienti.size();
			if (s.startsWith("-") || s.startsWith("+")) {
				for (int i = 0; i < size; i++) {
					Monom m = new Monom(coeficienti.get(i), puteri.get(i + 1));
					rez.monoame.add(m);
				}
			} else {
				for (int i = 0; i < size; i++) {
					Monom m = new Monom(coeficienti.get(i), puteri.get(i));
					rez.monoame.add(m);
				}
			}
		} catch (Exception e) {
			System.out.println("Nu e ok!!");
			return null;
		}
		return rez;
	}

	public StringBuilder polynomialToMessage() {
		StringBuilder s = new StringBuilder();
		DecimalFormat df = new DecimalFormat("0.0");

		for (int i = 0; i < this.monoame.size(); i++) {
			int grad = this.monoame.get(i).getGrad();
			float coeficient = this.monoame.get(i).getValoare();
			coeficient = Float.parseFloat(df.format(coeficient));
			if (i == 0) {
				if ((coeficient == 1 && grad == 0) || (grad == 0)) {
					s.append(coeficient + " ");
				} else if (coeficient == 1 || coeficient == -1) {
					s.append(Math.signum(coeficient) + "x^" + grad);
				} else if (coeficient == 0) {
					s.append(" ");
				} else
					s.append(coeficient + "x^" + grad);
			} else {
				if ((coeficient == 1 && grad == 0) || (grad == 0)) {
					s.append(" + " + coeficient + " ");
				} else if (coeficient == 1 || coeficient == -1) {
					s.append(" + " + Math.signum(coeficient) + "x^" + grad);
				} else if (coeficient == 0) {
					s.append(" ");
				} else
					s.append(" + " + coeficient + "x^" + grad);
			}
		}
		return s;
	}
}
