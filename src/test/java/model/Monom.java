package model;
public class Monom {

	private float valoare;
	private int grad;

	public float getValoare() {
		return valoare;
	}

	public void setValoare(float valoare) {
		this.valoare = valoare;
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int f) {
		this.grad = f;
	}

	public Monom(float f, int g) {
		this.valoare = f;
		this.grad = g;
	}
}
