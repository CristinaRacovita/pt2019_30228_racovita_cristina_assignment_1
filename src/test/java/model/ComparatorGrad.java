package model;

import java.util.Comparator;

class ComparatorGrade implements Comparator<Monom> {
	public int compare(Monom a, Monom b) {
		return -a.getGrad() + b.getGrad();
	}
	
}
