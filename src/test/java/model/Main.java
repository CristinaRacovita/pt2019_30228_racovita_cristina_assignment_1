package model;

import vedere.View;
import control.Controller;

public class Main {

	public static void main(String[] args) {

		View v = new View();
		Controller c = new Controller(v);
		v.setVisible(true);

	}

}
